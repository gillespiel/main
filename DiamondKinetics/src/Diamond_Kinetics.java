import java.io.*;
import java.util.*;

public class Diamond_Kinetics {
	
	public static void main(String[] args) throws IOException{
		
		Scanner scan = new Scanner(System.in);
		String f = scan.next();
		//String f = "/Users/lucindagillespie/git/DiamondKinetics/bin/latestSwing.csv";
		BufferedReader input = new BufferedReader((new FileReader(new File(f))));
		ArrayList<String[]> rows = new ArrayList<String[]>();
		String line;
		
		while((line = input.readLine())!=null){
			String[] tmp = line.split(",");
			rows.add(tmp);
		}
		
		//DATA STRUCTURE: a two dimensional array that takes the values in the columns of the csv file
		//				  as its rows. This way, when a method is called, data[row] can be inputed into
		//				  the method call, and the method will iterate through an array containing data 
		// 				  of the same type. In other words, if it was desired that the method be called 
		// 				  on the x accelerometer values, data[1] would contain soley those values. 
		double[][] data = new double[7][rows.size()];
		
		for(int r = 0; r < rows.size();r++){
			String[] arr = rows.get(r);

			for(int c = 0; c < rows.get(0).length;c++){
				data[c][r] = Double.parseDouble(arr[c]);
			}
		}
		
		
		//EXAMPLE METHOD USE:
		
		//use method to check x accelerometer values between index 0 and 1275
		System.out.println(searchContinuityAboveValue(data[1], 0, 1275, 2.65, 10));
	}
	
	/**
	 * helper
	 * @param data : an array that represents a column of the csv file
	 * @param indexBegin: index to begin search
	 * @param indexEnd: index to end search
	 * @param thresholdLo: floor value for comparing elements in specified array
	 * @param thresholdHi: ceiling value for comparing elements in specified array or Max integer if there is no ceiling threshold
	 * @param winLength: number of values that must be consecutive for return index to be valid
	 * @return first index of array where data has winLength number of consecutive values above thresholdLo and below thresholdHi
	 */
	public static int helper(double[] data, int indexBegin, int indexEnd, double thresholdLo, double thresholdHi, int winLength){
		int count=0;
		int index=-1;
		for(int i = indexBegin; i<= indexEnd; i++){
			if(data[i] >=thresholdLo && data[i] <= thresholdHi){
				if(count==0)
					index = i;
				
				count ++;
			}
			
			else{
				if(count>0){
					if(count!=winLength)
						count = 0;
				}
			}
			
			if(count >= winLength)
				break;
		}
		
		if(count >=winLength)
			return index;
		
		else return -1;
		
	}
	
	
	/**
	 * searchContinuityAboveValue
	 * @param data: an array that represents a column of the csv file
	 * @param indexBegin: index to begin search
	 * @param indexEnd: index to end search
	 * @param threshold: floor value for comparing elements in specified array
	 * @param winLength: number of values that must be consecutive for return index to be valid
	 * @return first index of array where data has winLength number of consecutive values above threshold
	 */
	public static int searchContinuityAboveValue(double[] data, int indexBegin, int indexEnd, double threshold, int winLength){
		
		
		if(indexBegin< 0 || indexEnd >= data.length){
			return -1;
		}
		
		return helper(data, indexBegin, indexEnd, threshold, Integer.MAX_VALUE, winLength);
	}
	
	/**
	 * backSearchContinuityWithinRange
	 * @param data: an array that represents a column of the csv file
	 * @param indexBegin: index to begin search
	 * @param indexEnd: index to end search
	 * @param thresholdLo: floor value for comparing elements in specified array
	 * @param thresholdHi: ceiling value for comparing elements in specified array
	 * @param winLength: number of values that must be consecutive for return index to be valid
	 * @return first index of array where data has winLength number of consecutive values above thresholdLo and below thresholdHi
	 */
	public static int backSearchContinuityWithinRange(double[] data, int indexBegin, int indexEnd, double thresholdLo, double thresholdHi, int winLength){
		
		if(indexBegin< 0 || indexEnd >= data.length){
			return -1;
		}
		
		return helper(data, indexBegin, indexEnd, thresholdLo, thresholdHi, winLength);
	}
	
	/**
	 * searchContinuityAboveValueTwoSignals
	 * @param data1: an array that represents a column of the csv file
	 * @param data2: an array that represents a column of the csv file
	 * @param indexBegin: index to begin search
	 * @param indexEnd: index to end search
	 * @param threshold1: floor value for comparing elements in data1
	 * @param threshold2: floor value for comparing elements in data2
	 * @param winLength: number of values that must be consecutive for return index to be valid
	 * @return first index of array where data has winLength number of consecutive values above threshold in both data1 and data2
	 */
	public static int searchContinuityAboveValueTwoSignals(double[] data1, double[] data2, int indexBegin, int indexEnd, double threshold1, double threshold2, int winLength){
		int index1 = -1;
		int index2 = -2;
		int i = 0;
		
		if(indexBegin< 0 || indexEnd >= data1.length || indexEnd>= data2.length){
			return -1;
		}
		
		while((index1!=index2)&&(i<data1.length)){
			
			index1=helper(data1, indexBegin, indexEnd, threshold1, Integer.MAX_VALUE, winLength);
			
			index2 = helper(data2, indexBegin, indexEnd, threshold2, Integer.MAX_VALUE, winLength);
			
			if(index1==index2){
				return index1;
			}
			
			i++;
		}
		
		
		return -1;
	}
	
	/**
	 * searchMultiContinuityWithinRange
	 * @param data: an array that represents a column of the csv file
	 * @param indexBegin: index to begin search
	 * @param indexEnd: index to end search
	 * @param thresholdLo: floor value for comparing elements in specified array
	 * @param thresholdHi: ceiling value for comparing elements in specified array
	 * @param winLength: number of values that must be consecutive for return index to be valid
	 * @return array containing both the first index of array where data has winLength number of consecutive values above threshold, 
	 * 		   and the index of the last consecutive value
	 */
	public static ArrayList<int[]> searchMultiContinuityWithinRange(double[] data, int indexBegin, int indexEnd, double thresholdLo, double thresholdHi, int winLength){
		int index1 = -1;
		int index2 = -2;
		int count = 0;
		ArrayList<int[]> ans = new ArrayList<>();
		
		if(indexBegin< 0 || indexEnd >= data.length){
			//int[] outBounds = {-1, -1};
			return ans;
		}
		
		while(indexBegin < data.length){
			
		for(int i = indexBegin; i<= indexEnd; i++){
			if(data[i] >=thresholdLo && data[i] <= thresholdHi){
				if(count==0)
					index1 = i;
				
				count ++;
			}
			
			else{
				if(count>0){
					if(count!=winLength)
						count = 0;
					
					else
						index2 = i;
				}
			}
			
			if(count >= winLength)
				break;
			
		}
		
		if(count >= winLength){
			int[] range = {index1, index2};
			ans.add(range);
			indexBegin = index1+1;
		}
		
		else{
			indexBegin++;
		}
			
		count = 0;
			
	}
		
		return ans;
					
	}
}
